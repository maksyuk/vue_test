import api from '../../api'

const state = {
  users: [],
  isLoadUsers: null
}

// getters
const getters = {
  cartProducts: (state, getters, rootState) => {
    return state.items.map(({ id, quantity }) => {
      const product = rootState.products.all.find(product => product.id === id)
      return {
        title: product.title,
        price: product.price,
        quantity
      }
    })
  }
}

// actions
const actions = {
  getUsers ({ commit }) {
    commit('setIsLoadUsersList', true)
    api.users.getUsers().then(
      (response) => {
        commit('setUsers', response.data.results)
        commit('setIsLoadUsersList', false)
      },
      () => {
        commit('setIsLoadUsersList', false)
      }
    )
  }
}

// mutations
const mutations = {
  setIsLoadUsersList (state, status) {
    state.isLoadUsers = status
  },
  setUsers (state, users) {
    state.users = users
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
