import { get } from 'axios'
import { API_URL } from '../constants'

const getUsers = () => (get(API_URL))

export default { getUsers }
